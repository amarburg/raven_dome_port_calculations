# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.5.2
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

import math
import pandas as pd
import numpy as np

# +
## All measurements in mm
inch_to_mm = 25.4

## Lens parameters
lens_f = 4 #mm

flange_back = 17.5     # for c-mount
entrance_pupil = 0     # Use zero for now
lens_length = 30       # Overall length from flange, from data sheet

## Dome parameters
#   For Sexton 3.4" dome

r1 = 1.7*inch_to_mm
d = 0.15*inch_to_mm
r2 = r1-d

## Material properties
# Refractive indices
nw = 1.34    ## fresh water
nd = 1.5  ## acrylic from https://www.filmetrics.com/refractive-index-database/Acrylic/Acrylate-Lucite-Perspex-Plexiglass
na = 1.0 ## air

## "Rule of thumb" calculation
# 3*radius - 4*thickness from apex
rot = 3*r1-4*d
print("Rule of thumb vfpp = %f mm (%f in) from apex" % (rot, rot/inch_to_mm))
quoted = 3.902 ## inches from apex
print("Sexton quotes %f mm (%f in) from apex" % (quoted*inch_to_mm, quoted))

# +
## From https://www.scubageek.com/articles/wwwdome.html
# Primary focal length of dome's external surface
f1 = nw*(r1/(nd-nw))

# Secondary focal length of dome's external surface
f1p = f1*nd/nw

# Primary focal length of dome's internal surface
f2p = nd*(r2/(na-nd))

# Second focal length of dome's internal surface
f2pp=f2p*na/nd

enovrf=nd/f1p + na/f2pp - (d/f1p)*(na/f2pp)

# primary focal length of the dome
f=nw/enovrf             

# secondary focal length of the dome
fpp=f*na/nw       

# position of the dome's primary focal plane relative
# to the dome's external vertex (on the dome axis)
a1f=-f*(1.-d/f2p) 

# position of the dome's secondary focal plane relative
# to the dome's internal vertex (on the dome axis)
a2fpp=fpp*(1.-d/f1p)

# position of the dome's primary principal plane relative
# to the dome's external vertex (on the dome axis)
a1h=f*d/f2p

# position of the dome's secondary principal plane relative
# to the dome's internal vertex (on the dome axis)
a2hpp=-fpp*d/f1p 

## Location of the secondary focal plane relative to the apex of
# the dome
vfpp = a2fpp+d
print("Secondary focal point: %f mm (%f in) from apex" % (vfpp, vfpp/inch_to_mm) )


def calc_virtual( p ):
    # p=object distance relative to external vertex
    # s=object distance relative to primary principal plane
    s=p+a1h

    # image distance relative to secondary principal plane
    spp=na/(nw/f - nw/s)    

    # image distance relative to external vertex
    ppp=spp+a2hpp+d  
    
    return ppp

def calc_real( ppp ):
#     if ppp <= vfpp:
#         return math.inf
    
    spp = ppp-a2hpp-d
    s = nw/( nw/f - na/spp )
    return np.where( ppp <= vfpp, math.inf, s-a1h)
    
    # size of image relative to size of object
    #m=-(nw/na)*(spp/s)      
    
    
#g=distance from dome principal plane to lens primary
#  principal plane
#view=tangent of lens' effective half-angle of view
#     divided by tangent of lens' in-air half-angle
#     of view
#view=-(spp-g)/(s+g)/m

# +
p = 1e6
ppp = calc_virtual( p )
print("Real world %f at virtual %f from apex of dome" % (p, ppp))

pa = calc_real(ppp)
print("Virtual %f at real %f" % (ppp, pa))


# +
## Depth of field calculations

def hyperfocal( aperture, f=lens_f, coc=0.007 ):
    return f*f / (aperture*coc) + f

## DOF calculations are at image plane

def dof_near( focal_distance, aperture, f=lens_f ):
    hf = hyperfocal(aperture, f )
    return (hf-f)*focal_distance / ( hf+focal_distance-2*f) 
    
def dof_far( focal_distance, aperture, f=lens_f ):
    hf = hyperfocal(aperture, f )
    return (hf-f)*focal_distance / ( hf-focal_distance) 
    
def fd_given_dof_far( dof_far, aperture, f=lens_f ):
    hf = hyperfocal(aperture, f )
    return dof_far * (hf)/(hf-f+dof_far)

def dof_near_for_fd_at_hyperfocal( aperture, f=lens_f ):
    hf = hyperfocal( aperture, f )
    return dof_near( hf, aperture, f )

def fd_given_dof_near( n, aperture, f=lens_f ):
    hf = hyperfocal(aperture, f )
    return (hf - 2*f)*n / (hf-f-n)


# +
aperture = 4.0

print("Hyperfocal distance %f mm" % hyperfocal(aperture))


fd = 100
print("For an fd of %f mm, the DOF will be %f to %f mm" % (fd, dof_near(fd, aperture), dof_far(fd, aperture)) )

df = 107.887797

print("For an dof_far of %d, the focal distance should be %f" % (df, fd_given_dof_far(df, aperture)))

# +
apertures = { "aperture": [1.8, 2.0, 2.8, 4.0, 5.6, 8.0]}

data = pd.DataFrame(apertures)

# data['hyperfocal_mm'] = hyperfocal(data['aperture'])
# data['dof_near_hyperfocal'] = dof_near_for_fd_at_hyperfocal(data['aperture'])

vfpp_rel_image_plane = abs(vfpp)+r1+entrance_pupil+flange_back - 1e-6
data['dof_far'] = vfpp_rel_image_plane
data['fd'] = fd_given_dof_far( vfpp_rel_image_plane, data['aperture'] )
data['dof_near'] = dof_near( data['fd'], data['aperture'] )

data['dof_near_rel_apex'] = data['dof_near'] -r1-entrance_pupil-flange_back
data['dof_far_rel_apex'] = data['dof_far'] -r1-entrance_pupil-flange_back

data['dof_near_rel_apex_water'] = calc_real( -data['dof_near_rel_apex'] )
data['dof_far_rel_apex_water'] = calc_real( -data['dof_far_rel_apex'] )

data.set_index('aperture', inplace=True )


print(data)

# +
## Same as above, but set def_near to 120cm

data = pd.DataFrame(apertures)

#vfpp_rel_image_plane = abs(vfpp)+r1+entrance_pupil+flange_back - 1e-6
data['dof_near'] = 120
data['fd'] = fd_given_dof_near( data['dof_near'], data['aperture'] )
data['dof_far'] = dof_far( data['fd'], data['aperture'] )

data['dof_near_rel_apex'] = data['dof_near'] -r1-entrance_pupil-flange_back
data['dof_far_rel_apex'] = data['dof_far'] -r1-entrance_pupil-flange_back

data['dof_near_rel_apex_water'] = calc_real( -data['dof_near_rel_apex'] )
data['dof_far_rel_apex_water'] = calc_real( -data['dof_far_rel_apex'] )



data.set_index('aperture', inplace=True )


print(data)


# +
# Convert vfpp from apex of dome to image plane.

vfpp_rel_image_plane = abs(vfpp)+r1+entrance_pupil+flange_back
vfpp_rel_lens_front  = abs(vfpp)+r1+entrance_pupil - lens_length

print("Assuming lens protrudes %.2f mm into dome" % (lens_length-entrance_pupil))
print("vfpp of %f mm from the apex is %f from image plane" % (vfpp, vfpp_rel_image_plane))
print("vfpp of %f mm from the apex is %f from front of the lens" % (vfpp, vfpp_rel_lens_front))

fd_fvpp = fd_given_dof_far(vfpp_rel_image_plane, aperture)
print("At aperture %f, a focal distance %.2f mm from the image plane spans a dof [%.2f,%.2f] mm from the image plane" % (aperture, fd_fvpp, dof_near(fd_fvpp,aperture), dof_far(fd_fvpp,aperture)))

dof_near_rel_front_of_lens = dof_near(fd_fvpp,aperture)-(lens_length+flange_back)
dof_far_rel_front_of_lens = dof_far(fd_fvpp, aperture)-(lens_length+flange_back)
fd_fvpp_rel_front_of_lens = fd_fvpp - (lens_length+flange_back)
print("This is %.2f from the front of the lens with a dof [%.2f,%.2f] mm" % (fd_fvpp_rel_front_of_lens,dof_near_rel_front_of_lens,dof_far_rel_front_of_lens) )

## Translate back to apex
dof_near_rel_apex = dof_near(fd_fvpp,aperture)-r1-entrance_pupil-flange_back
dof_far_rel_apex = dof_far(fd_fvpp, aperture)-r1-entrance_pupil-flange_back

print("This DOF is [%f,%f] mm (%f,%f in) relative to the dome apex" % (dof_near_rel_apex, dof_far_rel_apex,dof_near_rel_apex/inch_to_mm, dof_far_rel_apex/inch_to_mm))
print("Which maps to [%f,%f] mm underwater from the dome apex" % (calc_real(-dof_near_rel_apex), calc_real(-dof_far_rel_apex)))

# +
f_min = 100
fd_min = 100+flange_back
print("Minimum focal length for the lens of %.2f from flange is %.2f from image plane" % (f_min, fd_min))
print("DOF from image plane [%.2f,%.2f]" % (dof_near(fd_min,aperture),dof_far(fd_min,aperture)))

dof_near_rel_apex = dof_near(fd_min,aperture)-r1-entrance_pupil-flange_back
dof_far_rel_apex = dof_far(fd_min,aperture)-r1-entrance_pupil-flange_back

print("Which has a DOF of [%.2f,%.2f] from the dome apex" % (dof_near_rel_apex,dof_far_rel_apex))
print("Which maps to [%f,%f] mm underwater from the dome apex" % (calc_real(-dof_near_rel_apex), calc_real(-dof_far_rel_apex)))

# -

# ## 

#
